# This script generates n number of html maps for top n critical trees
# Inputs:
#   -	tree_based_aggregation.csv
#   -	top_deciles_critical_trees.csv
# Outputs:
#   -	n .html maps for top n critical.


import numpy as np
import pandas as pd
import csv
import folium
from folium.plugins import HeatMap
import random
import datetime
import re
import os

def main(mainPath,clusterGapInMeter):
    #mainPath='/Users/arashheidarian/Documents/VECTOR/data'
    #clusterGapInMeter=50
    path=mainPath+'/output/'+str(clusterGapInMeter)+'_meter_gap/'

    #-- Read data frome CSV  ------------------------
    criticalTreesDF=pd.read_csv(path+"top_deciles_critical_trees.csv",header=0)
    clustersDF=pd.read_csv(path+"subclusters_monthToAccident.csv",header=0)

    if (len(criticalTreesDF.index) ==0): print("No Top Critical Tree found in top_deciles_critical_trees.csv - Adjust thresholds!")
    if(len(criticalTreesDF.index)>0):

        clustersDF=clustersDF[['Lat','Lon','cluster_SAIDI_decile','TreeSpeciesName','months_to_accident','TreeQuantity','CTNID']] # 'CTNID' is tree index. We use this to avoide double/triple counting number of trees that have been duplicated due to their multiple matches to multiple powerpoles in one sub-clusters. Good example is location [-36.37412112,174.7986867]
        clustersDF=clustersDF[clustersDF['months_to_accident']>0]
        clustersDF[['TreeSpeciesName']]=clustersDF['TreeSpeciesName'].str.lower()
        clustersDF['Lat_Lon']=clustersDF['Lat'].map(str)+' '+clustersDF['Lon'].map(str)

        criticalTreesList=criticalTreesDF['TreeSpeciesName'].tolist()

        for t in range(len(criticalTreesList)):

            #--Define the map--------
            map = folium.Map(location=[-36.881522, 174.781016], zoom_start=12, tiles='openstreetmap')
            fg = folium.FeatureGroup(name=criticalTreesList[t])

            #--Aggeragate to identify each point size on the map
            theTreeDF=clustersDF[clustersDF['TreeSpeciesName']==criticalTreesList[t]]

            theTreeDF = theTreeDF.groupby(['CTNID','Lat_Lon','cluster_SAIDI_decile']).TreeQuantity.mean().reset_index() # 'CTNID' is tree index. We use this to avoide double/triple counting number of trees that have been duplicated due to their multiple matches to multiple powerpoles in one sub-clusters. Good example is location [-36.37412112,174.7986867]
            #treeCountDFAgg = treeCountDFAgg.groupby(['TreeSpeciesName']).agg({'TreeQuantity': np.sum})

            treeAggDF=theTreeDF.groupby(['Lat_Lon'])\
                                 .agg({'cluster_SAIDI_decile':np.median
                                       ,'TreeQuantity': np.sum
                                       })
            treeAggDF['Lat_Lon']=treeAggDF.index
            # We create decile to scale the size of points on map, to show bigger points on map, where thre are are more trees
            treeAggDF['point_size'] = pd.cut(treeAggDF['TreeQuantity'], 10,labels=False)
            treeAggDF['point_size']=treeAggDF['point_size']+2

            # first separate Lat and Lon again.
            treeAggDF['Lat'] = treeAggDF['Lat_Lon'].map(lambda x:x.split()[0])
            treeAggDF['Lon'] = treeAggDF['Lat_Lon'].map(lambda x: x.split()[1])

            #replace NaN SAIDI with 0
            treeAggDF['cluster_SAIDI_decile']=treeAggDF['cluster_SAIDI_decile'].map(lambda x: float(x) if str(x).replace('.','').isdigit() else 0)

            # Lets create list of lat, lon and other attributes as input for the map
            treeAggList=treeAggDF[['Lat','Lon','point_size','TreeQuantity','cluster_SAIDI_decile']].values.tolist()
            # Index: 0 Lat
            #        1 Lon
            #        2 point_size
            #        3 TreeQuantity
            #        4 cluster_SAIDI_decile
            for p in range (len(treeAggList)):
                popupHtml = '<br>' + str(treeAggList[p][3]) + ' trees <br> SAIDI_Decile: ' + str(treeAggList[p][4])
                if(treeAggList[p][4]==0):
                    fg.add_child(folium.RegularPolygonMarker([float(treeAggList[p][0]), float(treeAggList[p][1])], popup=popupHtml,weight=1, fill_color='blue',fill_opacity= 0.3 ,opacity=1,number_of_sides=5, radius=treeAggList[p][2], color='blue'))
                else: fg.add_child(folium.RegularPolygonMarker([float(treeAggList[p][0]), float(treeAggList[p][1])], popup=popupHtml,weight=1, fill_color='red',fill_opacity= (treeAggList[p][4]/10)+0.1, opacity=1 ,number_of_sides=5, radius=treeAggList[p][2], color='red'))
                map.add_child(fg)

                # folium.Circle(
                #     location=[float(treeAggList[p][0]), float(treeAggList[p][1])],
                #     popup=popupHtml,
                #     radius=treeAggList[p][2],
                #     color='crimson',
                #     fill=True,
                #     fill_color='crimson'
                # ).add_to(map)

            # HEATMAP
            locSAIDIDF = treeAggDF[['Lat', 'Lon', 'cluster_SAIDI_decile']].astype(float)
            locSAIDIList = locSAIDIDF.values.tolist()
            HeatMap(locSAIDIList).add_to(map)

            # Save the map
            map.save(path + criticalTreesList[t] + "_map.html")

if __name__ == "__main__":
    main()
import numpy as np
import pandas as pd
import csv
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from numpy import median
import matplotlib as mpl


#--DISTRIBUTION CHART-------------------------------------------------
def distribution(mainPath,clusterGapInMeter,type):

    # mainPath = '/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'
    # clusterGapInMeter = 50
    # type='quantile' # 'quartile','quantile','normal'


    path = mainPath + '/output/' + str(clusterGapInMeter) + '_meter_gap/'

    # -- Read data frome CSV ------------------------
    survivalDF = pd.read_csv(path + "survival_rate.csv", header=0)


    #--DRAWING CHARTS---------------------
    plt.figure()
    if(type=='quantile')  :
        chart=sns.jointplot(x="prevent_success_rate_quantile", y="months_passed_quantile", data=survivalDF, kind="kde")
        fileName='quantile_success_rate_distribution'
    if(type == 'quartile'):
        chart=sns.jointplot(x="prevent_success_rate_quartile", y="months_passed_quartile", data=survivalDF, kind="kde")
        fileName = 'quartile_success_rate_distribution'
    if(type == 'normal')  :
        chart=sns.jointplot(x="prevent_success_rate", y="months_passed", data=survivalDF, kind="kde")
        fileName = 'success_rate_distribution'

    chart.savefig(path + fileName+'.png')

    #plt.clf()

#--SCATER PLOT-------------------------------------------------------------------
def scaterplot(mainPath, clusterGapInMeter, type):
    # mainPath = '/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'
    # clusterGapInMeter = 50
    # type = 'quantile'  # 'quartile','quantile','normal'

    path = mainPath + '/output/' + str(clusterGapInMeter) + '_meter_gap/'

    # -- Read data frome CSV ------------------------
    survivalDF = pd.read_csv(path + "survival_rate.csv", header=0)

    #--DRAWING CHARTS---------------------
    plt.figure()
    if(type=='quantile')  :
        chart=sns.lmplot(x="prevent_success_rate", y="months_passed_quantile", data=survivalDF, order=2, ci=None,scatter_kws={"s": 80})
        fileName='quantile_scaterplot_success_rate'
    if(type == 'quartile'):
        chart=sns.lmplot(x="prevent_success_rate", y="months_passed_quartile", data=survivalDF, order=2, ci=None,scatter_kws={"s": 80})
        fileName = 'quartile_scaterplot_success_rate'
    if(type == 'normal')  :
        chart=sns.lmplot(x="prevent_success_rate", y="months_passed"         , data=survivalDF, order=2, ci=None, scatter_kws={"s": 80})
        fileName = 'scaterplot_success_rate'

    chart.savefig(path + fileName+'.png')
    #plt.clf()


#--BAR CHART------------------------------------------------------
def barchart(mainPath, clusterGapInMeter, type):
    # mainPath = '/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'
    # clusterGapInMeter = 50
    # type = 'quantile'  # 'quartile','quantile','normal'

    path = mainPath + '/output/' + str(clusterGapInMeter) + '_meter_gap/'

    # -- Read data frome CSV ------------------------
    survivalDF = pd.read_csv(path + "survival_rate.csv", header=0)

    # --DRAWING CHARTS---------------------
    plt.figure()
    if (type == 'quantile'):
        chart = sns.barplot(x="months_passed_quantile", y="prevent_success_rate", data=survivalDF, estimator=median)
        fileName = 'quantile_barchart_median_success_rate'
    if (type == 'quartile'):
        chart = sns.barplot(x="months_passed_quartile", y="prevent_success_rate", data=survivalDF, estimator=median)
        fileName = 'quartile_barchart_median_success_rate'
    if (type == 'normal'):
        chart = sns.barplot(x="months_passed", y="prevent_success_rate", data=survivalDF, estimator=median)
        fileName = 'scaterplot_barchart_median_success_rate'

    chart.get_figure().savefig(path + fileName + '.png')

    #plt.clf()






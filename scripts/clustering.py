# This script:
#        - clusters the Event Points and CTN Points based on Long and Lat using DBSCAN
#        - identifies top n clusters based on some of SAIDI index of datapoints in each cluster
#        - maps the clusters on map and creates heatmap over the map based on SAIDI index of each datapoint
#        - creates deciles based on total SAIDI of each cluster
#        - creates deciles based on individual SAIDI index
#        - creates csv file with built deciles and clusters added
#
# inpout: - vegFault.csv
#         - vtn.csv
#         - Customer_Impact.csv
#
# Output: - clusterList.csv : compraising all datapoints from  vegFault.csv with added deciles and clusters per each datapoint
#         - aggergated_clusters.csv : list of all created clusters and sum of SAIDI per each, followed by the deciles.
#         - mergedFaultNImpact.csv: merged vegFault.csv and vtn.csv. This is used as a main dataset in the entire script.
#         - m_cluster_map.html.html : which is the map with clusters marked and heatmap.
#
# Notes: Update on this version:Nothing new, just the clustering job is done by calling geoClusterDBSCAN function in  clusteringFunction.py .

from sklearn.metrics.pairwise import pairwise_distances
from sklearn.cluster import DBSCAN
from geopy.distance import vincenty
import numpy as np
import pandas as pd
import csv
import folium
import random
import datetime
from folium import IFrame
from folium.plugins import HeatMap
import branca
import re
import os
from clusteringFunction import geoClusterDBSCAN

def main(completeAnalysis,
         topNrows,
         CTN_from_date,
         clusterGapInMeter,
         topSAIDIClusterDecile,
         topCusAffectedClusterDecile,
         topTreesQuantityClusterDecile,
         topnClustersMarkedOnMap,
         mainPath,
         high_risk_cluster_SAIDI_decile,
         high_risk_cluster_customers_affected_decile,
         high_risk_cluster_treeQuantity_decile,
         medium_risk_cluster_SAIDI_decile_upper_bond,
         medium_risk_cluster_SAIDI_decile_lower_bond,
         medium_risk_cluster_customers_affected_decile,
         low_risk_cluster_SAIDI_decile,
         low_risk_cluster_customers_affected_decile):

    # CTN_from_date='2012-06-01'
    # clusterGapInMeter=50
    # topSAIDIClusterDecile=(9,8)
    # topCusAffectedClusterDecile=(9,8,7,6,5)
    # topTreesQuantityClusterDecile=(0,1,2,3,4,5,6,7,8,9) # decile 0 contains minimum of 1 tree
    # topnClustersMarkedOnMap=20
    # completeAnalysis=True
    # topNrows=1000 # if completeAnalysis set to False, this param comes handy.
    # mainPath='/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'

    dirPath=mainPath+'/output/'+str(clusterGapInMeter)+'_meter_gap/'

    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    inputPath=mainPath+'/input/'
    outPath=mainPath+'/output/'+str(clusterGapInMeter)+'_meter_gap/'



    def distance_in_meters(x, y):
        return vincenty((x[0], x[1]), (y[0], y[1])).m

    def dmy_to_ymd(strDate):
        #converts '1/6/13' to '2013-01-06'
        matchObj = re.match(r'(.*)/(.*)/(.*)', strDate, re.M | re.I)
        month = matchObj.group(1) if len(matchObj.group(1)) == 2 else '0' + matchObj.group(1)
        day = matchObj.group(2) if len(matchObj.group(2)) == 2 else '0' + matchObj.group(2)
        year = '20' + matchObj.group(3)
        date = year + '-' + month + '-' + day

        return date

    #-- Read data frome CSV ------------------------
    print("Data Preparation...")
    # Fault Vegitation
    faultDF=pd.read_csv(inputPath+"vegFault.csv",header=0)
    faultDF['eventDateFormatted']=faultDF['Date'].map(lambda x:dmy_to_ymd(x))
    faultDF['SAIDI']=faultDF['SAIDI'].map(lambda x: float(x) if str(x).replace('.','').isdigit() else 0)
    faultDF['nonzero_SAIDI_count']=faultDF['SAIDI'].map(lambda x: 1 if x>0 else 0)
    faultDF['FeederName']=faultDF['HVFeeder'].astype(str)
    faultDF['event_count']=1
    faultDF['ctn_count']=0

    faultDF=faultDF.drop('Date',axis=1)
    faultDF=faultDF.drop('SRNumber',axis=1)
    faultDF=faultDF.drop('HVFeeder',axis=1)
    faultDF=faultDF.drop('Address',axis=1)
    faultDF=faultDF.drop('Suburb',axis=1)
    faultDF=faultDF.drop('pole#',axis=1)

    # Impcated Customers
    customerImpactDF=pd.read_csv(inputPath+"Customer_Impact.csv",header=0)
    customerImpactDF['SAIDI_calculated_manually']=(customerImpactDF['Max_Outage_Duration']*customerImpactDF['Customers_Affected'])/customerImpactDF['NetworkCustomerCount']
    customerImpactDF['HVEventID']=customerImpactDF['HVEventID'].astype(str) # change it to str to make it joinable with faultDF

    # CTN
    ctnDF=pd.read_csv(inputPath+"ctn.csv",header=0)
    ctnDF=ctnDF[['TreeSpeciesName','TreeQuantity','VoltageCode','IsHistoryFlag','PoleLatitudeValue', 'PoleLongitudeValue','FeederName','CTNIssueDate','TreeNotificationCompletionFullDate','WorkRequiredDescription','CTNID']]
    ctnDF['Lat']=ctnDF[['PoleLatitudeValue']].values
    ctnDF['Lon']=ctnDF[['PoleLongitudeValue']].values
    ctnDF['CTNIssueDateFormatted']=ctnDF['CTNIssueDate'].map(lambda x:x[:10])
    ctnDF['CTNIssueDateFormatted']=pd.to_datetime(ctnDF['CTNIssueDateFormatted'])
    ctnDF=ctnDF[ctnDF['CTNIssueDateFormatted']>=CTN_from_date]

    ctnDF[['TreeQuantity']]=ctnDF[['TreeQuantity']].replace(0,1)
    ctnDF['event_count']=0
    ctnDF['ctn_count']=1

    ctnDF=ctnDF.drop('PoleLatitudeValue',axis=1)
    ctnDF=ctnDF.drop('PoleLongitudeValue',axis=1)

    # limit number of rows to get some results quickly , just for testing purposes.
    if completeAnalysis==False:
        ctnDF=ctnDF.head(topNrows)
        faultDF=faultDF.head(topNrows)

    #-- Append CTN to faultDF-----
    # -- it helps to cluster  ctn and fault veg data together. So we will be able to see where the accident has happened and which cut and trims have happend around the same area.
    faultDF=pd.concat([ctnDF,faultDF],ignore_index=True)
    faultDF= faultDF[pd.notnull(faultDF['Lat'])] #drop nan lon and lat
    faultDF=faultDF[faultDF['Lat']!=0] #drop 0 lan and lat


    #ctnDF['FormattedDate']=ctnDF['TreeNotificationCompletionFullDate'].map(lambda x:x[:10])

    #--Joining faultDF and Impact Customers  & Imputing missing SAIDI for event_count=1 rows only -----------------------------
    print("Data Imputation...")

    faultDF=pd.merge(faultDF,customerImpactDF,on='HVEventID',how='left')
    faultDF['SAIDI_imputed']= faultDF[faultDF['event_count']==1][['SAIDI_calculated_manually','SAIDI']]  .sum(axis=1).where(faultDF['SAIDI']==0,faultDF['SAIDI'])

    # Give SAIDI=0 for CTN data (where ctn_count=1)
    faultDF[['SAIDI_imputed']]=faultDF[['SAIDI_imputed']].replace('NaN',0)

    # Replace other Nan values
    #faultDF[['ctn_count']]=faultDF[['ctn_count']].replace('NaN',0)
    #faultDF[['event_count']]=faultDF[['event_count']].replace('NaN',0)
    faultDF=faultDF.replace('NaN',0)

    faultDF.reset_index(drop=True, inplace=True)
    faultDF['index']=faultDF.index

    # Create a list of lists for locations and SAIDI for HeatMaponly
    faultLocDF=faultDF[['Lat','Lon']].astype(float)
    faultLocList=faultLocDF.values.tolist()

    #-- CREATE DISTANCE MATRIX-------------------------------------------------------------------

    result=geoClusterDBSCAN(faultDF,clusterGapInMeter)

    # Aggregations to get some stats around built clusters

    SAIDIClusterDF=result.groupby('cluster')\
                         .agg({'SAIDI_imputed':np.sum
                               ,'cluster':np.size
                               ,'Customers_Affected':np.sum
                               ,'TreeQuantity': np.sum
                               })
    SAIDIClusterDF['clusters']=SAIDIClusterDF.index
    SAIDIClusterDF.columns=['cluster_size','sum_SAIDI_cluster','sum_TreeQuantity','sum_Customers_Affected','cluster']
    #SAIDIClusterDF=SAIDIClusterDF[SAIDIClusterDF['sum_SAIDI_cluster']>0]

    #Sorting
    sortedSAIDIClusterDF=SAIDIClusterDF.sort_values(by='sum_SAIDI_cluster',ascending=False)
    topnClusters=sortedSAIDIClusterDF['cluster'].head(topnClustersMarkedOnMap).values.tolist() # will be used as a reference to mark the top n clusters on map


    #--Create Decile-------
    print("Creating Deciles...")
    sortedSAIDIClusterDF['cluster_SAIDI_decile_enabled']             =(sortedSAIDIClusterDF['sum_SAIDI_cluster']!=0     ) & (sortedSAIDIClusterDF['cluster']!='no cluster')
    sortedSAIDIClusterDF['cluster_customers_affected_decile_enabled']=(sortedSAIDIClusterDF['sum_Customers_Affected']!=0) & (sortedSAIDIClusterDF['cluster']!='no cluster')
    sortedSAIDIClusterDF['cluster_treeQuantity_decile_enabled']      =(sortedSAIDIClusterDF['sum_TreeQuantity']!=0      ) & (sortedSAIDIClusterDF['cluster']!='no cluster')


    sortedSAIDIClusterDF['cluster_SAIDI_decile']             =pd.qcut(sortedSAIDIClusterDF[sortedSAIDIClusterDF['cluster_SAIDI_decile_enabled']             ==True]['sum_SAIDI_cluster']     ,10,labels=False)
    sortedSAIDIClusterDF['cluster_customers_affected_decile']=pd.qcut(sortedSAIDIClusterDF[sortedSAIDIClusterDF['cluster_customers_affected_decile_enabled']==True]['sum_Customers_Affected'],10,labels=False)
    sortedSAIDIClusterDF['cluster_treeQuantity_decile']      =pd.qcut(sortedSAIDIClusterDF[sortedSAIDIClusterDF['cluster_treeQuantity_decile_enabled']      ==True]['sum_TreeQuantity']      ,10,labels=False)
    #result['individual_SAIDI_decile']=pd.qcut(result['SAIDI_imputed'],10,labels=False)
    #result['individual_SAIDI_decile']=pd.qcut(result.rank(method='first'),10)

    #Join two DFs
    mergedResults=pd.merge(result,sortedSAIDIClusterDF,on='cluster')

    #--Write all clusters and the sum of SAIDI
    sortedSAIDIClusterDF     .to_csv(outPath+"aggergated_clusters.csv",header=True,index=False)
    mergedResults            .to_csv(outPath+"clusterList.csv",header=True,index=False)
    faultDF                  .to_csv(outPath+"mergedFaultNImpact.csv",header=True,index=False)

    #==========================================================
    #=========================================================
    #-- MAPPING - -------------------------------
    print("Mapping...")

    def rgb(minimum, maximum, value):
        minimum, maximum = float(minimum), float(maximum)
        ratio = 2 * (value-minimum) / (maximum - minimum)
        b = int(max(0, 255*(1 - ratio)))
        r = int(max(0, 255*(ratio - 1)))
        g = 255 - b - r
        return r, g, b

    clusterSAIDIDF=SAIDIClusterDF[['cluster','sum_SAIDI_cluster']].values.tolist()

    for col in range(len(clusterSAIDIDF)):
        color = '#%02X%02X%02X' % rgb(SAIDIClusterDF[['sum_SAIDI_cluster']].min(), SAIDIClusterDF[['sum_SAIDI_cluster']].max(), float(clusterSAIDIDF[col][1]))
        clusterSAIDIDF[col].append(color)

    # First create a background map
    map=folium.Map(location=[-36.881522, 174.781016], zoom_start=12,tiles='openstreetmap')

    # HEATMAP
    locSAIDIDF=result[['Lat','Lon','SAIDI_imputed']].astype(float)
    locSAIDIList=locSAIDIDF.values.tolist()
    HeatMap(locSAIDIList).add_to(map)
    cell_coordinate=mergedResults[['Lat','Lon','cluster','SAIDI_imputed','cluster_SAIDI_decile','cluster_customers_affected_decile','Customers_Affected','ctn_count','event_count','TreeQuantity','TreeSpeciesName','cluster_treeQuantity_decile']].sort_values(by='cluster',ascending=False).values.tolist()

    # If you want to consider a list of feature write this command
    fg=folium.FeatureGroup(name="My map")

    # create a loop to put all of your markers on the map
    cntr=0
    import random
    r = lambda: random.randint(0, 255)
    color = '#%02X%02X%02X' % (r(), r(), r())
    cell_coordinate.sort(key=lambda x: x[2])
    list_len=len(cell_coordinate)
    event_count=0
    SAIDISum=0
    customersAffected=0
    TreeQuantity=0

    redList=[]
    greenList=[]
    blueList=[]


    locations=[]
    for i in cell_coordinate:
        #if cntr>=1000: break

        if(cntr>0 and cell_coordinate[cntr][2]!=cell_coordinate[cntr-1][2]):
             # print(cell_coordinate[cntr - 1][2])
             # print(str(members) + ' members')
             # print(locations)
             locations.sort(key=lambda x:x[0])
             #if(cell_coordinate[cntr - 1][2]!='no cluster' and cell_coordinate[cntr - 1][2] in topnClusters): #this is based on top n pre-defined number
             if(cell_coordinate[cntr - 1][2]!='no cluster' and cell_coordinate[cntr - 1][4] in topSAIDIClusterDecile and cell_coordinate[cntr - 1][5] in topCusAffectedClusterDecile  and len(locations)>0): # this is based on top decile
                 # finding the midpoint in each cluster for marker
                 lat = []
                 long = []
                 for l in locations:
                     lat.append(l[0])
                     long.append(l[1])
                 html=cell_coordinate[cntr - 1][2] + '<br>' + str(event_count) + ' events reported <br> SAIDI:'+str(SAIDISum)+ '<br> Affected '+str(customersAffected) +' customers <br>' + str(TreeQuantity) +' trees affected'

                 # High Risk Cluster-----
                 # cell_coordinate[cntr - 1][4]  is cluster_SAIDI_decile
                 # cell_coordinate[cntr - 1][5]  is cluster_customers_affected_decile
                 # cell_coordinate[cntr - 1][11] is cluster_treeQuantity_decile
                 if(cell_coordinate[cntr - 1][4] in high_risk_cluster_SAIDI_decile and
                    cell_coordinate[cntr - 1][5] in high_risk_cluster_customers_affected_decile and
                    cell_coordinate[cntr - 1][11] in high_risk_cluster_treeQuantity_decile
                    ):
                     folium.Marker([sum(lat) / len(lat),sum(long) / len(long)]
                                   ,popup=html
                                   ,icon = folium.Icon(color='red')
                                   ).add_to(map)
                     redList.append(cell_coordinate[cntr - 1][2])

                 # Medium Risk Cluster-----
                 # cell_coordinate[cntr - 1][4]  is cluster_SAIDI_decile
                 # cell_coordinate[cntr - 1][5]  is cluster_customers_affected_decile
                 # cell_coordinate[cntr - 1][11] is cluster_treeQuantity_decile
                 elif (     (cell_coordinate[cntr - 1][4] in medium_risk_cluster_SAIDI_decile_upper_bond )
                        or (cell_coordinate[cntr - 1][4] in medium_risk_cluster_SAIDI_decile_lower_bond and cell_coordinate[cntr - 1][5]>=medium_risk_cluster_customers_affected_decile)
                       ):
                     folium.Marker([sum(lat) / len(lat), sum(long) / len(long)]
                                   , popup=html
                                   , icon=folium.Icon(color='green')
                                   ).add_to(map)
                     greenList.append(cell_coordinate[cntr - 1][2])

                 # Low Risk Cluster-----
                 # cell_coordinate[cntr - 1][4]  is cluster_SAIDI_decile
                 # cell_coordinate[cntr - 1][5]  is cluster_customers_affected_decile
                 # cell_coordinate[cntr - 1][11] is cluster_treeQuantity_decile
                 elif (cell_coordinate[cntr - 1][4] in low_risk_cluster_SAIDI_decile and  cell_coordinate[cntr - 1][5] in low_risk_cluster_customers_affected_decile):
                     folium.Marker([sum(lat) / len(lat), sum(long) / len(long)]
                                   , popup=html
                                   , icon=folium.Icon(color='blue')
                                   ).add_to(map)
                     blueList.append(cell_coordinate[cntr - 1][2])
                 # else:
                 #     folium.Marker([sum(lat) / len(lat), sum(long) / len(long)]
                 #                   , popup=html
                 #                   , icon=folium.Icon(color='white')
                 #                   ).add_to(map)

                             #print (circleLoc)

             # print("=========================================")
             locations=[]
             event_count = 0
             SAIDISum=0
             customersAffected=0
             TreeQuantity=0
             r = lambda: random.randint(0, 255)
             color = '#%02X%02X%02X' % (r(), r(), r())

        if( (cntr>0        and cell_coordinate[cntr][2]==cell_coordinate[cntr-1][2]) or
            (cntr<list_len and cell_coordinate[cntr][2]==cell_coordinate[cntr+1][2])
           ):
            event_count = event_count+i[8]
            SAIDISum=SAIDISum+i[3]
            TreeQuantity=TreeQuantity+i[9]
            customersAffected=customersAffected+i[6]

            if(i[8]==1): # if its a powerpole
                popupHtml=str(i[2])+'<br> SAIDI '+str(i[3])
                if cell_coordinate[cntr - 1][2] != 'no cluster':
                    fg.add_child(folium.RegularPolygonMarker([float(i[0]),float(i[1])],popup=popupHtml, fill_color=color, number_of_sides=4, radius=4,color=color))
                else: fg.add_child(folium.RegularPolygonMarker([float(i[0]),float(i[1])],popup=popupHtml, fill_color=color, number_of_sides=4, radius=1,color=color)) # we give smaller size to outliers(no cluster)

            elif(i[7]==1): # if its a tree
                popupHtml=str(i[2])+'<br>'+str(i[9])+' trees <br> Species: '+str(i[10])
                if cell_coordinate[cntr - 1][2] != 'no cluster':
                    fg.add_child(folium.RegularPolygonMarker([float(i[0]),float(i[1])],popup=popupHtml, fill_color=color, number_of_sides=5, radius=4,color=color))
                else: fg.add_child(folium.RegularPolygonMarker([float(i[0]),float(i[1])],popup=popupHtml, fill_color=color, number_of_sides=5, radius=1,color=color))# we give smaller size to outliers(no cluster)

            map.add_child(fg)
            locations.append([float(i[0]),float(i[1])])
            #print(locations)


        cntr = cntr + 1
    map.save(outPath+str(clusterGapInMeter)+"m_cluster_map.html")

#
# del(faultDF)
# del(faultLocDF)
# del(faultLocList)
# del(SAIDIClusterDF)
# del(tempDF)
if __name__ == "__main__":
    main()

def geoClusterDBSCAN(geoDF,clusterGapInMeter):

    from sklearn.metrics.pairwise import pairwise_distances
    from sklearn.cluster import DBSCAN
    from geopy.distance import vincenty
    import numpy as np
    import pandas as pd

    def distance_in_meters(x, y):
        return vincenty((x[0], x[1]), (y[0], y[1])).m

    # Due to probable operations, indices might have been disordered. We need a clean ordered index to join the clusters to this df.
    geoDF.reset_index(drop=True, inplace=True)
    geoDF['reset_index'] = geoDF.index

    # Create a list of lists for locations and SAIDI for HeatMaponly
    geoLocDFDF=geoDF[['Lat','Lon']].astype(float)
    geoLocDFList=geoLocDFDF.values.tolist()
    
    #-- CREATE DISTANCE MATRIX-------------------------------------------------------------------
    print("Creating Distance Matrix...")
    #geoLocDFList=([-36.9771413769632,174.897339327932],[-46.18913006913,169.740406362447])
    #print("Building Distance Matrix...")
    distance_matrix = pairwise_distances((geoLocDFList), metric=distance_in_meters)
    
    
    #-- CLUSTER DISTANCE MATRIX USING DBSCAN-----------------------------------------------------
    print("DBSCAN Clustering...")
    dbscan = DBSCAN(metric='precomputed', eps=clusterGapInMeter, min_samples=2, n_jobs=-1)
    dbscan.fit(distance_matrix)
    labels = dbscan.labels_
    #get number of clusters
    no_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    
    #--CREATE A DATAFRAME OF CLUSTERS AND THEIR MEMBERS
    print("Finalising...")
    #print("CREATING A DATAFRAME OF CLUSTERS AND THEIR MEMBERS...")
    clusters_df=pd.DataFrame(columns=['cluster','reset_index'],index=None)
    clusters_indices=[]
    clusterNumber=0
    counter = 0
    for i in range(0,no_clusters):
        clusterNumber+=1
        cluster_members=list(np.nonzero(labels == i)[0])
        for j in range(len(cluster_members)):
            tempDF=pd.DataFrame({'cluster':['cluster '+str(clusterNumber)],
                                 'reset_index':[cluster_members[j]]},
                                index=[counter]
                    )
            counter+=1
            clusters_df=pd.concat([clusters_df,tempDF])
            #print(clusterNumber)
    
    #print clusters_df
    #print('-------------------------')
    
    #-- JOIN clusters_df and geoDF
    #result=clusters_df.merge(msisdn_index_df,on=('msisdn_index'))
    #print ("Writing the results...")
    columns=list(geoDF.columns.values)
    columns.append('cluster')
    result=pd.merge(clusters_df,geoDF,how='right',on='reset_index')[columns]
    result['cluster']=result['cluster'].replace(np.nan, 'no cluster', regex=True)

    return result

#-------------------------------------------------------------------------------------
def geoClusterKmeans(geoDF,numberOfClusters):
    from sklearn.metrics.pairwise import pairwise_distances
    from sklearn.cluster import KMeans
    from geopy.distance import vincenty
    import numpy as np
    import pandas as pd

    def distance_in_meters(x, y):
        return vincenty((x[0], x[1]), (y[0], y[1])).m

    # Shuffle the dataframe for Kmeans
    geoDF = geoDF.sample(frac=1)

    # Due to probable operations, indices might have been disordered. We need a clean ordered index to join the clusters to this df.
    geoDF.reset_index(drop=True, inplace=True)
    geoDF['reset_index']=geoDF.index

    # Create a list of lists for locations and SAIDI for HeatMaponly
    geoLocDFDF = geoDF[['Lat', 'Lon']].astype(float)
    geoLocDFList = geoLocDFDF.values.tolist()

    # -- CREATE DISTANCE MATRIX-------------------------------------------------------------------
    # msisdn_loc=([-36.9771413769632,174.897339327932],[-46.18913006913,169.740406362447])
    # print("Building Distance Matrix...")
    distance_matrix = pairwise_distances((geoLocDFList), metric=distance_in_meters)

    # -- CLUSTER DISTANCE MATRIX USING DBSCAN-----------------------------------------------------
    # print("Clustering...")
    kmeans = KMeans(n_clusters=numberOfClusters, random_state=1,max_iter=300, tol=0.0001,n_jobs=-1 )
    kmeans.fit(distance_matrix)
    labels = kmeans.labels_
    # get number of clusters
    no_clusters = len(set(labels)) - (1 if -1 in labels else 0)

    # --CREATE A DATAFRAME OF CLUSTERS AND THEIR MEMBERS
    # print("CREATING A DATAFRAME OF CLUSTERS AND THEIR MEMBERS...")
    clusters_df = pd.DataFrame(columns=['cluster', 'reset_index'], index=None)
    clusters_indices = []
    clusterNumber = 0
    counter = 0
    for i in range(0, no_clusters):
        clusterNumber += 1
        cluster_members = list(np.nonzero(labels == i)[0])
        for j in range(len(cluster_members)):
            tempDF = pd.DataFrame({'cluster': ['cluster ' + str(clusterNumber)],
                                   'reset_index': [cluster_members[j]]},
                                  index=[counter]
                                  )
            counter += 1
            clusters_df = pd.concat([clusters_df, tempDF])
            # print(clusterNumber)

    # print clusters_df
    # print('-------------------------')

    # -- JOIN clusters_df and geoDF
    # result=clusters_df.merge(msisdn_index_df,on=('msisdn_index'))
    # print ("Writing the results...")
    columns = list(geoDF.columns.values)
    columns.append('cluster')
    result = pd.merge(clusters_df, geoDF, how='right', on='reset_index')[columns]
    result['cluster'] = result['cluster'].replace(np.nan, 'no cluster', regex=True)

    return result
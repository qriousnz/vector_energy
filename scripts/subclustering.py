# This script:
#        - Crates subclusters using K-Means clustering.
#        - Calculates month difference between last tree data and the closest accident in each subcluster.
#
# inpout: - clusterList.csv generated by clustering0.4.1.py
#
# Output: - subclusters_monthToAccident.csv : its same as clusterList.csv with extra columns showing subclusters and months to accident (for tree related rows)
#
# Notes:


import numpy as np
import pandas as pd
import csv
import folium
import random
import datetime
import re
import os
from clusteringFunction import geoClusterKmeans

def main(mainPath,clusterGapInMeter,includeNoClusters,hasNoTreeNotificationCompletionFullDate):

    # mainPath='/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'
    # clusterGapInMeter=50
    # includeNoClusters = True
    # hasNoTreeNotificationCompletionFullDate=True

    path=mainPath+'/output/'+str(clusterGapInMeter)+'_meter_gap/'

    #-- Read data frome CSV ------------------------
    clusterDF=pd.read_csv(path+"clusterList.csv",header=0)
    clusterDF['TreeSpeciesName'] = clusterDF['TreeSpeciesName'].str.lower()


    #--Select clusters with trees in them only--------------
    clusterAgg=clusterDF.groupby('cluster')\
                         .agg({'ctn_count':np.sum
                               ,'event_count':np.sum
                               })
    clustersWithTreesList=list(clusterAgg[clusterAgg['ctn_count']>0].index)
    clustersWithEventList=list(clusterAgg[clusterAgg['event_count']>0].index)
    clustersWithEventAndTreeList=list(set(clustersWithTreesList).intersection(clustersWithEventList))

    clusterDF['withTreeAndEventCluster']=clusterDF['cluster'].map(lambda x:1 if x in clustersWithEventAndTreeList  else 0)

    clusterWithTreesAndEventsDF=clusterDF[clusterDF['withTreeAndEventCluster']==1]

    #-----Create Sub-clusters-------------

    if includeNoClusters: clustersList= list(set(clusterWithTreesAndEventsDF['cluster'].values.tolist()))
    else                : clustersList= list(set(clusterWithTreesAndEventsDF[clusterWithTreesAndEventsDF['cluster']!='no cluster']['cluster'].values.tolist()))


    #clustersList=['cluster 2708']

    mainSubClusterMonthToAccidentDf=pd.DataFrame(columns=[ 'CTNID','CTNIssueDate', 'CTNIssueDateFormatted', 'FeederName',
                                                           'HVEventComments/SRDescription', 'HVEventID',
                                                           'HVFaultCause/ServiceRequestSubType',
                                                           'HVFaultType/ServiceRequestType', 'IsHistoryFlag', 'Lat', 'Lon',
                                                           'SAIDI', 'SRActivityComments', 'TreeNotificationCompletionFullDate',
                                                           'TreeQuantity', 'TreeSpeciesName', 'VoltageCode',
                                                           'WorkRequiredDescription', 'ctn_count', 'eventDateFormatted_x',
                                                           'event_count', 'nonzero_SAIDI_count', 'Customers_Affected',
                                                           'NetworkCustomerCount', 'Max_Outage_Duration',
                                                           'SAIDI_calculated_manually', 'SAIDI_imputed', 'index',
                                                           'cluster_size', 'sum_SAIDI_cluster', 'sum_TreeQuantity',
                                                           'sum_Customers_Affected', 'cluster_SAIDI_decile_enabled',
                                                           'cluster_customers_affected_decile_enabled',
                                                           'cluster_treeQuantity_decile_enabled', 'cluster_SAIDI_decile',
                                                           'cluster_customers_affected_decile', 'cluster_treeQuantity_decile',
                                                           'withTreeAndEventCluster', 'main_cluster', 'reset_index',
                                                           'sub_cluster', 'eventDateFormatted_y', 'months_to_accident','maintained_months_ago'
                                                         ],index=None)

    for cl in range(len(clustersList)):
        mainClusterDF=clusterWithTreesAndEventsDF[clusterWithTreesAndEventsDF['cluster']==clustersList[cl]]
        mainClusterDF['main_cluster']=mainClusterDF[['cluster']]
        mainClusterDF = mainClusterDF.drop('cluster', axis=1)

        #--Define number of clusters based on number of power pole accidents events
        clusterCountDF=mainClusterDF.groupby('main_cluster').agg({'event_count': np.sum})
        numberOfClusters=list(clusterCountDF['event_count'].values)[0]

        #--If we have only 1 powerpole event, we need to give Kmeans a chance of detecting outliers if there is any. So in this case number of clusters should set to 2. If there is no outlier, and, tree and pole are overlapped or are very close, KMeans still creates 1 cluster, eventhough 2 is given as number of clusters.
        if (numberOfClusters==1): numberOfClusters+=1

        #--K-Means Clustering------------------------
        result = geoClusterKmeans(mainClusterDF, numberOfClusters)
        result['sub_cluster']=result[['cluster']]
        result = result.drop('cluster', axis=1)
        #result=geoClusterDBSCAN(mainClusterDF,2)
        #result.to_csv(path+'subCluster.csv')


        # Get some stats and get list of clusters
        aggClusterDF = result.groupby('sub_cluster') \
            .agg({'ctn_count': np.sum
                     , 'sub_cluster': np.size
                     , 'event_count': np.sum
                  })
        aggClusterDF['clusters'] = aggClusterDF.index
        aggClusterDF.columns = ['sum_ctn','cluster_size',  'sum_events','sub_cluster']
        subclusterList=list(aggClusterDF['sub_cluster'].values)

        for c in range (len(subclusterList)):
            subclusterDF=result[result['sub_cluster']==subclusterList[c]]


            #---Find closest date between CTN and Event------

            #-drop rows with no event date and ctn events. And only continue if both >0
            CTNIssueDateDF =subclusterDF[['CTNIssueDateFormatted']]
            CTNIssueDateDF = CTNIssueDateDF.dropna(axis=0, how='any')
            eventDatedDF =subclusterDF[['eventDateFormatted']]
            eventDatedDF = eventDatedDF.dropna(axis=0, how='any')

            # Only work with subclusters that have at least 1 CTN date and 1 event date
            if(len(eventDatedDF)>0 and len(CTNIssueDateDF)>0):
                subClusterCTNIssueDateDF = pd.to_datetime(list(CTNIssueDateDF['CTNIssueDateFormatted'].values))
                subClusterCTNIssueDateDF = pd.DataFrame({'CTNIssueDateFormatted':subClusterCTNIssueDateDF})
                subClusterCTNIssueDateDF = subClusterCTNIssueDateDF.sort('CTNIssueDateFormatted')#.reset_index(level=1)

                subClustereventDatedDF = pd.to_datetime(list(eventDatedDF['eventDateFormatted'].values))
                subClustereventDatedDF = pd.DataFrame({'eventDateFormatted':subClustereventDatedDF})
                subClustereventDatedDF = subClustereventDatedDF.sort('eventDateFormatted')#.reset_index(level=1)

                currentDate = [(datetime.datetime.now())]
                currentDateDF = pd.DataFrame(currentDate, columns=['current_date'])

                # This calculates number of months from closest CTNIssueDateFormatted date to eventDateFormatted ONLY if CTNIssueDateFormatted happened before eventDateFormatted.
                closestEventAndCTNMatchDF=pd.merge_asof(subClustereventDatedDF,subClusterCTNIssueDateDF,left_on='eventDateFormatted',right_on='CTNIssueDateFormatted')
                closestEventAndCTNMatchDF['months_to_accident']= ((closestEventAndCTNMatchDF.eventDateFormatted - closestEventAndCTNMatchDF.CTNIssueDateFormatted)/ np.timedelta64(1, 'M'))

                # This calculates number of months from  CTNIssueDateFormatted date to current date.
               # if (closestEventAndCTNMatchDF['CTNIssueDateFormatted'].dropna().shape[0]>0): # if there is any CTNIssuedate available

                #closestEventAndCTNMatchDF=closestEventAndCTNMatchDF.sort('CTNIssueDateFormatted')
                currentDateAndCTNMatchDF=pd.merge_asof(currentDateDF,subClusterCTNIssueDateDF,left_on='current_date',right_on='CTNIssueDateFormatted')
                currentDateAndCTNMatchDF['maintained_months_ago']= ((currentDateAndCTNMatchDF.current_date - currentDateAndCTNMatchDF.CTNIssueDateFormatted)/ np.timedelta64(1, 'M'))                # Continue with the rest of the process of there is any month_diff_moth available in the subcluster
                currentDateAndCTNMatchDF['current_date']=currentDateAndCTNMatchDF['current_date'].astype(str)

                closestEventAndCTNMatchDF=pd.merge(closestEventAndCTNMatchDF,currentDateAndCTNMatchDF,how='left',on='CTNIssueDateFormatted')

                if(len(closestEventAndCTNMatchDF[closestEventAndCTNMatchDF['months_to_accident']>=0])>0):
                    closestEventAndCTNMatchDF['CTNIssueDateFormatted'] =closestEventAndCTNMatchDF['CTNIssueDateFormatted'].astype(str)
                    subclusterDF             ['CTNIssueDateFormatted'] = subclusterDF            ['CTNIssueDateFormatted'].astype(str)

                    tempSubclusterMonthToAccDF=pd.merge(subclusterDF,closestEventAndCTNMatchDF,on='CTNIssueDateFormatted',how='left')

                    mainSubClusterMonthToAccidentDf=pd.concat([mainSubClusterMonthToAccidentDf,tempSubclusterMonthToAccDF])

                    #print(subclusterDF[['sub_cluster']])
                    #print(subClustereventDatedDF)
                    #print(subClusterCTNIssueDateDF)
                    # print(closestEventAndCTNMatchDF)
                    # print(subclusterMonthToAccDF)
                    # print("---------------------")

    if(hasNoTreeNotificationCompletionFullDate):
        mainSubClusterMonthToAccidentDf=mainSubClusterMonthToAccidentDf[mainSubClusterMonthToAccidentDf['TreeNotificationCompletionFullDate'] == '1900-01-01 00:00:00.000']
    mainSubClusterMonthToAccidentDf.to_csv(path+'subclusters_monthToAccident.csv',index=False)

if __name__ == "__main__":
    main()
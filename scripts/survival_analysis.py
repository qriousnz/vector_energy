# This script:
#        -  Reads subclusters_monthToAccident.csv and subclusters_preventiveMaintainance_monthsAgo.csv, joins these CSV files based on Tree Type and Months_ago/Months_to_accident to build joinedDF
#        -  joinedDF is used to do survival analysis to measure success rate over time.
#
# inpout: - subclusters_monthToAccident.csv
#         - subclusters_preventiveMaintainance_monthsAgo.csv
#
# Output: - survival_rate.csv # we use this for building graphs and plots for further analysis
#         - least_effective_preventions_TreeMonthBased.csv
#         - tree_based_prevention_agg.csv
#         - least_effective_preventions_TreeBased.csv
#
# Notes:


import numpy as np
import pandas as pd
import csv
import folium
import random
import datetime
import re
import os
from clusteringFunction import geoClusterKmeans

def main(mainPath,clusterGapInMeter,total_tree_threshold,prevent_success_rate_decile,ignoreNmonthsPass):
    # mainPath='/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'
    # clusterGapInMeter=50
    #
    # total_tree_threshold=1 #>=1
    # prevent_success_rate_decile=5 #<=5
    # ignoreNmonthsPass=9 # as the CTN data covers only 41 records from last 9 months, we need to ignore the matches between 9 months_ago_maintained and 9 months_to_accident. Because we have lot of  months_to_accident=9 but not only few months_ago_maintained=9


    path=mainPath+'/output/'+str(clusterGapInMeter)+'_meter_gap/'

    #--Read data frome CSV ------------------------
    subclustersDF           = pd.read_csv(path + "subclusters_monthToAccident.csv",header=0)
    preventiveSubclustersDF = pd.read_csv(path + "subclusters_preventiveMaintainance_monthsAgo.csv", header=0)

    #-- Convert treeSpecies to lower case----
    subclustersDF['TreeSpeciesName'] = subclustersDF['TreeSpeciesName'].str.lower()
    preventiveSubclustersDF['TreeSpeciesName'] = preventiveSubclustersDF['TreeSpeciesName'].str.lower()

    #--Add suffix to columns of preventive
    preventiveSubclustersDF.columns=[str(col) + '_preventive' for col in preventiveSubclustersDF.columns]

    #--Add CTN_Type column -----
    preventiveSubclustersDF['CTN_Type']='preventive'
    subclustersDF['CTN_Type']='incident'

    #--Remove unneccassary rows ---------------------
    subclustersDF=subclustersDF[subclustersDF['TreeSpeciesName'].notnull()] #  keep only rwos with TreeSpeciesName
    preventiveSubclustersDF = preventiveSubclustersDF[preventiveSubclustersDF['TreeSpeciesName_preventive'].notnull()]  # keep only rwos with TreeSpeciesName

    subclustersDF=subclustersDF[subclustersDF['months_to_accident']>0]
    preventiveSubclustersDF = preventiveSubclustersDF[preventiveSubclustersDF['maintained_months_ago_preventive'] > 0]

    #--Round month_to_accident and maintained_months_ago_preventive
    subclustersDF.months_to_accident=subclustersDF.months_to_accident.round()
    #subclustersDF.maintained_months_ago = subclustersDF.maintained_months_ago.round()
    preventiveSubclustersDF.maintained_months_ago_preventive=preventiveSubclustersDF.maintained_months_ago_preventive.round()

    #--Aggregate----
    subclustersDFAgg = subclustersDF.groupby(['TreeSpeciesName', 'months_to_accident']).TreeQuantity.sum().reset_index()
    preventiveSubclustersDFAgg = preventiveSubclustersDF.groupby(['TreeSpeciesName_preventive', 'maintained_months_ago_preventive']).TreeQuantity_preventive.sum().reset_index()

    #--Create dummy column for joining purpose
    subclustersDFAgg          ['treeMonth'] = subclustersDFAgg          ['TreeSpeciesName']           .map(str) + ' ' + subclustersDFAgg          ['months_to_accident']              .map(str)
    preventiveSubclustersDFAgg['treeMonth'] = preventiveSubclustersDFAgg['TreeSpeciesName_preventive'].map(str) + ' ' + preventiveSubclustersDFAgg['maintained_months_ago_preventive'].map(str)

    #subclustersDFAgg.to_csv(path+'incident.csv')
    #preventiveSubclustersDFAgg.to_csv(path+'prevent.csv')
    #--JOIN DFs--------------------
    joinedDF=pd.merge(preventiveSubclustersDFAgg,subclustersDFAgg,how='inner',on='treeMonth')

    #--Outer join create lot of blank/nan TreeQuantity and TreeQuantity_preventive. Wefill them by 0
    joinedDF['TreeQuantity_preventive']=joinedDF['TreeQuantity_preventive'].fillna(0)
    joinedDF['TreeQuantity'] = joinedDF['TreeQuantity'].fillna(0)
    #joinedDF['months_to_accident'] = joinedDF['months_to_accident'].fillna(-1)
    #joinedDF['maintained_months_ago_preventive'] = joinedDF['maintained_months_ago_preventive'].fillna(-1)

    #--Build total_tree & prevent_success_rate
    joinedDF['total_tree']=joinedDF['TreeQuantity']+joinedDF['TreeQuantity_preventive']
    joinedDF['prevent_success_rate']=joinedDF['TreeQuantity_preventive']/joinedDF['total_tree']
    #joinedDF['months_passed']=joinedDF[['months_to_accident','maintained_months_ago_preventive']].apply(lambda x:x[0] if x[0]>=0 else x[1] ,axis=1)
    joinedDF['months_passed'] = joinedDF[['months_to_accident', 'maintained_months_ago_preventive']].apply(lambda x: x[0] if str(x[0])!='nan' else x[1], axis=1)

    #--Ignore data with ignoreNmonthsPass (9) months passed. See the reason in setting params section above.
    joinedDF=joinedDF[joinedDF['months_passed']>=ignoreNmonthsPass]

    #--Ignore prevent_success_rate of 0 and 1 if total_tree<10
    #joinedDF['dropThisRow']=joinedDF[['prevent_success_rate','total_tree']].apply(lambda x:1 if (x[0]==0 or x[0]==1) and x[1]<10 else 0 , axis=1)
    joinedDF['dropThisRow'] = joinedDF[['prevent_success_rate', 'total_tree']].apply(lambda x: 1 if (x[0] == 0 or x[0] == 1) else 0, axis=1)
    joinedDF=joinedDF[joinedDF['dropThisRow']==0]
    joinedDF=joinedDF.drop('dropThisRow',axis=1)


    #--Creating Deciles---
    joinedDF['total_tree_decile'] = pd.cut(joinedDF['total_tree'], 20, labels=False)
    joinedDF['prevent_success_rate_decile'] = pd.cut(joinedDF['prevent_success_rate'], 10, labels=False)
    joinedDF['prevent_success_rate_quantile'] = pd.cut(joinedDF['prevent_success_rate'], 3, labels=False)
    joinedDF['months_passed_quantile'] = pd.cut(joinedDF['months_passed'], 3, labels=False)
    joinedDF['prevent_success_rate_quartile'] = pd.cut(joinedDF['prevent_success_rate'], 4, labels=False)
    joinedDF['months_passed_quartile'] = pd.cut(joinedDF['months_passed'], 4, labels=False)

    #--Select least successfull preventions (Tree and Month based)---
    least_effective_preventions=joinedDF[joinedDF['total_tree_decile']>=total_tree_threshold]
    least_effective_preventions=least_effective_preventions[least_effective_preventions['prevent_success_rate_decile']<=prevent_success_rate_decile]

#-------------------------------------------------------------------------------------------------
#-----PART 2 ---- Define Least Effective Preventions Based On Tree Type ONLY----------------------
#-------------------------------------------------------------------------------------------------
    #--Data Aggregations
    joinedDFTreeBasedAgg = joinedDF.groupby('TreeSpeciesName')\
                                   .agg({'prevent_success_rate':'median','months_passed':'count','total_tree':'sum'})\
                                   .reset_index()\
                                   .rename(columns={'prevent_success_rate':'median_prevent_success_rate','months_passed':'months_record_count'})

    #--Creating Deciles -------
    joinedDFTreeBasedAgg['median_prevent_success_rate_decile']= pd.cut(joinedDFTreeBasedAgg['median_prevent_success_rate'], 10,labels=False)
    joinedDFTreeBasedAgg['months_record_count_decile']        = pd.cut(joinedDFTreeBasedAgg['months_record_count']        , 10, labels=False)
    joinedDFTreeBasedAgg['total_tree_decile']                 = pd.cut(joinedDFTreeBasedAgg['total_tree']                 , 10,labels=False)

    #--Selecting leastEffectiveTree-----
    leastEffectiveTree=joinedDFTreeBasedAgg[joinedDFTreeBasedAgg['median_prevent_success_rate_decile']<=5]
    leastEffectiveTree = leastEffectiveTree[leastEffectiveTree['months_record_count'] >= 3]
    leastEffectiveTree = leastEffectiveTree[leastEffectiveTree['total_tree_decile'] >= 0]


    joinedDF.to_csv(path+'survival_rate.csv')
    least_effective_preventions.to_csv(path+'least_effective_preventions_TreeMonthBased.csv')
    joinedDFTreeBasedAgg.to_csv(path+'tree_based_prevention_agg.csv')
    leastEffectiveTree.to_csv(path+'least_effective_preventions_TreeBased.csv')


if __name__ == "__main__":
    main()
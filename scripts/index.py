import subprocess
import os
import sys


import clustering
import subclustering
import subclustering_TreeNotificationCompletionFullDate
import subclustering_preventive_maintainance
import subclustering_preventive_maintainance_TreeNotificationCompletionFullDate
import tfidf_vectorize_description
import tree_based_deciles
import categorical_scaterplot
import mapping_critical_trees
import survival_analysis
import survival_charts


##########################################
###### SET PARAMETERS ####################
##########################################

#--clustering.py---------------------------
CTN_from_date='2012-06-01'
clusterGapInMeter=50
topSAIDIClusterDecile=(9,8)
topCusAffectedClusterDecile=(9,8,7,6,5)
topTreesQuantityClusterDecile=(0,1,2,3,4,5,6,7,8,9) # decile 0 contains minimum of 1 tree
topnClustersMarkedOnMap=20
completeAnalysis=True # for testin purposes to generate some results quickly. then topNrows also needs to be set .
topNrows=2000 # if completeAnalysis set to False, this param comes handy. It selects only top n rows from CTN and Fault Vegitation data for testin purposes to generate some results quickly.

#IF logic for high risk clusters (red markers):
#high_risk_cluster_SAIDI_decile AND
#high_risk_cluster_customers_affected_decile AND
#high_risk_cluster_treeQuantity_decile
high_risk_cluster_SAIDI_decile               =(9)
high_risk_cluster_customers_affected_decile  =(6,7,8,9)
high_risk_cluster_treeQuantity_decile        =(6,7,8,9)

#IF logic for medium risk clusters (green markers):
#(medium_risk_cluster_SAIDI_decile_upper_bond) OR
#(medium_risk_cluster_SAIDI_decile_lower_bond AND >=medium_risk_cluster_customers_affected_decile)
medium_risk_cluster_SAIDI_decile_upper_bond    = (9)
medium_risk_cluster_SAIDI_decile_lower_bond    = (8)
medium_risk_cluster_customers_affected_decile  = 8 # the threshold will be passed like >=8

#IF logic for low risk clusters (blue markers):
#low_risk_cluster_SAIDI_decile AND
#low_risk_cluster_customers_affected_decile
low_risk_cluster_SAIDI_decile               = (8)
low_risk_cluster_customers_affected_decile  = (0,1,2,3,4,5)
mainPath='/Users/arashheidarian/Documents/VECTOR/deliveredCodes/data'

#--SUBCLUSTERING------------------------------
# If this parameter is set to True so the TreeNotificationCompletionFullDate_subclustering and TreeNotificationCompletionFullDate_subclustering_maintainance need to set to false obviousely.
# This parameter is used to cover only rows with CTN Issue Date but no TreeNotificationCompletionFullDate
hasNoTreeNotificationCompletionFullDate=False
#--subclustering.py------------------------
includeNoClusters = True
TreeNotificationCompletionFullDate_subclustering=True # if it is set to false. CTNIssueDate will be used for subclustering and identifying critical trees

#--subclustering_preventive_maintainance.py------------------------
includeNoClusters_maintainance = True
TreeNotificationCompletionFullDate_subclustering_maintainance=True # if it is set to false. CTNIssueDate will be used for subclustering and identifying critical trees
monthsPassedTill='current_date' # 'current_date' or date in 'YYYY-MM-DD' format. If current_date is set, the model counts number of months since CTN issue date till today for survival analysis.

#--survival_analysis.py----------------------------
total_tree_threshold = 1  # >=1
prevent_success_rate_decile = 5  # <=5
ignoreNmonthsPass = 9  # as the CTN data covers only 41 records from last 9 months, we need to ignore the matches between 9 months_ago_maintained and 9 months_to_accident. Because we have lot of  months_to_accident=9 but not only few months_ago_maintained=9

#--tfidf_vectorize_description.py----------
# Read the documentaion carefully before changing any parameter here.
manualStopWordList = ['tree', 'power', 'notice', 'network', 'necessary', 'near', 'line', 'large', 'emergency', 'lv',
                      'hv', 'height', 'half', 'zone', 'restore', 'service', '11kv', 'clear', 'clearance', 'clerance',
                      'gain', 'nan', 'noticew', 'volt', 'vegetation', 'wire', 'wind', 'temove', 'washington']
topNKeywords = 15
selectFromSelectedKeywords = True  # if set true, it uses defined lists below, otherwise looks at top n keywords. Changing this to False may break the rest of scripts. See the documentations for more details.
trimPrunList = ['trimremove', 'trim', 'removetrim', 'prune']
removeList = ['remove', 'removal']
reduceList = ['reduction', 'reduce']
clearList = ['clerarance', 'clearn', 'cleareance', 'clearances']
stemList = ['stem']
branchList = ['branch', 'limb', 'lace']

#--tree_based_deciles.py------------------
tree_SAIDI_decile_threshold = 3  # >=3
tree_quantity_decile_threshold = 2  # >=2
uniqueLocationCount_decile_threshold = 4  # >=4
tree_months_to_accident_decile_threshold = 5  # <=5

#--categorical_scaterplot.py--------------
plotType='boxPlot' # available types: boxPlot, violinPlot

##########################################
###### EXECUTE SCRIPTS ###################
##########################################
#
#--clustering.py---------------------------
print("------CLUSTERING------")
clustering.main(completeAnalysis,
                 topNrows,
                 CTN_from_date,
                 clusterGapInMeter,
                 topSAIDIClusterDecile,
                 topCusAffectedClusterDecile,
                 topTreesQuantityClusterDecile,
                 topnClustersMarkedOnMap,
                 mainPath,
                 high_risk_cluster_SAIDI_decile,
                 high_risk_cluster_customers_affected_decile,
                 high_risk_cluster_treeQuantity_decile,
                 medium_risk_cluster_SAIDI_decile_upper_bond,
                 medium_risk_cluster_SAIDI_decile_lower_bond,
                 medium_risk_cluster_customers_affected_decile,
                 low_risk_cluster_SAIDI_decile,
                 low_risk_cluster_customers_affected_decile)
print("DONE!")

#--subclustering.py------------------------
print("------SUB-CLUSTERING------")
if TreeNotificationCompletionFullDate_subclustering:
    subclustering_TreeNotificationCompletionFullDate.main(mainPath,
                       clusterGapInMeter,
                       includeNoClusters,
                       hasNoTreeNotificationCompletionFullDate)

else: subclustering.main(mainPath,
                   clusterGapInMeter,
                   includeNoClusters,
                    hasNoTreeNotificationCompletionFullDate)

print("DONE!")

#--subclustering_preventive_maintainance.py------------------------

print("------SUB-CLUSTERING Preventive Maintainance------")
if TreeNotificationCompletionFullDate_subclustering_maintainance:
    subclustering_preventive_maintainance_TreeNotificationCompletionFullDate.main( mainPath,
                                                                                   clusterGapInMeter,
                                                                                   includeNoClusters_maintainance,
                                                                                   monthsPassedTill,
                                                                                   hasNoTreeNotificationCompletionFullDate)

else: subclustering_preventive_maintainance.main(mainPath,
                                                   clusterGapInMeter,
                                                   includeNoClusters_maintainance,
                                                    monthsPassedTill,
                                                    hasNoTreeNotificationCompletionFullDate)
print("DONE!")

#--survival_analysis.py----------------------------
print("------Survival Analysis------")
survival_analysis.main(mainPath,clusterGapInMeter,total_tree_threshold,prevent_success_rate_decile,ignoreNmonthsPass)

#--tfidf_vectorize_description.py----------
print("------TF-IDF------")
tfidf_vectorize_description.main(mainPath,
                                 clusterGapInMeter,
                                 manualStopWordList,
                                 topNKeywords,
                                 selectFromSelectedKeywords,
                                 trimPrunList,
                                 removeList,
                                 reduceList,
                                 clearList,
                                 stemList,
                                 branchList)
print("DONE!")

#--tree_based_deciles.py------------------
print("------TREE-BASED DECILE------")
tree_based_deciles.main(mainPath,
                        clusterGapInMeter,
                        tree_SAIDI_decile_threshold,
                        tree_quantity_decile_threshold,
                        uniqueLocationCount_decile_threshold,
                        tree_months_to_accident_decile_threshold)
print("DONE!")

#--categorical_scaterplot.py--------------
print("------DRAWING BOX PLOT------")
categorical_scaterplot.main(mainPath,
                            clusterGapInMeter,
                            plotType)
print("DONE!")

#--mapping_critical_trees.py--------------
print("------MAPPING CRITICAL TREES------")
mapping_critical_trees.main(mainPath,
                            clusterGapInMeter)
print("DONE!")

#--survival_charts.py
print("------DRAWING SUCCESS RATE CHARTS------")
#'quartile','quantile','normal'
survival_charts.distribution(mainPath,clusterGapInMeter,'normal')
survival_charts.scaterplot(mainPath, clusterGapInMeter, 'normal')
survival_charts.barchart(mainPath, clusterGapInMeter, 'normal')

survival_charts.distribution(mainPath,clusterGapInMeter,'quantile')
survival_charts.scaterplot(mainPath, clusterGapInMeter, 'quantile')
survival_charts.barchart(mainPath, clusterGapInMeter, 'quantile')

survival_charts.distribution(mainPath,clusterGapInMeter,'quartile')
survival_charts.scaterplot(mainPath, clusterGapInMeter, 'quartile')
survival_charts.barchart(mainPath, clusterGapInMeter, 'quartile')
print("DONE!")

print("------RESULTS ARE READY!------")
# README

## Introduction
Qrious have been engaged with Vector to understand and provide analysis on the tree maintenance and SAIDI data to provide Vector with insight into current trends as well as lay the foundation for more proactive and targeted tree maintenance using predictive analytics. This will help Vector retain high customer service standards, avoid fines and avoidable maintenance costs of fixing lines due to tree caused outages. 

## What is the model?
The created model uses DBSCAN algorithm to form the main clusters with 50 meters gap threshold and then creates sub-clusters using k-means algorithm to find closest trees and power poles in each cluster. The number of months from the work type (trimming/cutting/etc) till the closest power poles with reported accident in each sub-cluster is calculated and if there are multiple trees around a power pole in a sub-cluster, the tree with shortest interval to accident is selected as the one with highest possibility of causing the accident. The work type category is prepared using TF-IDF algorithm and other text-mining algorithms to remove stop words, punctuations, lemmatize and stem words to identify important keywords. Finally, trees in subclusters which have no incident are chosen as CTNs with high probability of being preventive maintainance. Then the combination of preventive and due-to-incident CTNs are used to do survival analysis. Survival analysis helps Vector to trace the success rate of their proactive CTN works over the time. As a result the model helps Vector :

-  To identify high risk areas, based on the created clusters. According to discussions between Qrious and Vector, Vector expressed their interest to see the results up to street level. This will cause to have high accuracy clustering up to one street or multiple close streets but also ending up with lots of outliers. To meet this requirement, the gap between clusters has been set to 50 meters. However with adjustable parameters provided in the scripts, it is possible to  change the scale up to region level by changing the gap up to 100 meters, 200 meters or more. The results are presented in CSV files and a map. The CSV files cover all the details about each cluster. The map shows all the clusters. Members of each cluster have the same colour and top n critical clusters/streets are marked with three colours. Red, green and blue colours on markers define high, medium and low level of  risk among selected top n critical clusters/streets.  More details can be view by clicking on markers. However the thresholds which define top n critical clusters/streets, can be easily adjusted in the scripts which gives more freedom to Vector to set the parameters based on their interests.   
-  To identify where, which type of tree and how many months  later since the tree has been trimmed/removed/etc (work type)  caused  the major accidents where SAIDI index is relatively high.    The outcome  is presented by generating CSV files, maps and box plots. The CSV files cover all the details related to number of months, median SAIDI indices, quantities and distribution across the country, all aggregated at tree type level. The model also generates n  maps for top n critical trees based on deciles created around the factors mentioned above. The size of points on the maps represents the quantity of trees and the colour opacity represents median SAIDI decile(at tree level). Points with having any SAIDI index associated to them can be found in red colour, and points with no SAIDI index linked to them are shown in blue colour. Consequently, the bigger points with higher opacities in red colour are the most critical areas where the tree caused major accidents.  All the thresholds which define top critical trees  are adjustable  which will be explained later in this document.  Finally, a  box plot gives a clear picture of accident distribution across the number of months based on the type of work (trim, branch removal, etc) for top critical trees. 
-  To trace the success rate of preventive maintainance job. The outcome can be accessed via a CSV file which can be used to draw different types of plots and graphs using available visualization tools. However Qrious provided different plots to Vector during the weekly presentations and is happy to automate the generating and saving-as-JPG  process for any desired plot. The outcome shows how the success rate fluctuates from proactive maintainance works since 60 months ago till 9 months ago. In order to measure the success rate, set of trees which caused incidents (with months-to-accident column) is joined to set of trees which have been detected as preventive maintainance CTNs (with months-ago column). The two datasets are joined on type of trees and months-ago/months-to-accident. Then the success rate for any particular tree on particular number of months is measured separatelt by getting the ration of preventive job per total number of trees ( total number of tree is sum of preventive CTNs and due-to-incident CTNs for particular tree on particular number of months). This process has been demonstrated in weekly presentations  with an example. 

The analysis looks at data since 60 months ago as the whole clustering and subclustering uses  the CTN data  since 2012 as preferred by Vector. However this number can be adjusted in SET PARAMS section in index.py. It also looks at data till 9 months ago because CTN file contains only 42 rows of CTN jobs done during last 8 months (as at 2017-12) while the subclustering process has detected lot of records with month-to-accident of 1 to 8 months. However this number also can be changed from 9 to any other number in  in SET PARAMS section in index.py. 
 
## Methodology

The  whole project is implemented through 6 scripts. In this chapter we  explain which algorithms have been used in each script and why they have be chosen.
All the scripts are called and executed via index.py . All the parameters can be set at this page. It is required to set mainPath variable to a folder which contains two subfolders, namely input and output. In input folder the following files should be copied. These are files Vector provided to Qrious in Excel XLSX format spreadsheet:  

- vegFault.csv
- ctn.csv
- Customer_Impact.csv

## Step 1 - clustering

### Inputs
These are files initially provided by vector: 

- vegFault.csv 
- ctn.csv 
- Customer_Impact.csv (this is used to impute missing SAIDI)

### Outputs 
-	50m_cluster_map.html (name changes based on defined meter gap. This is the map to view the clusters and their locations) 
-	aggergated_clusters.csv (it's a summary of all clusters with aggregated values per each cluster)
-	clusterList.csv (the list of all trees and power poles with the name of their clusters and summary of their clusters. This will be used in next scripts). 
-	mergedFaultNImpact.csv (this is list which is created after data preparation, imputation and merging CTN and VegFault dataset, and used as a main source of clustering. This is a list used as input to DBSCAN clustering.)

For forming the initial and main clusters we use DBSCAN algorithm. This algorithm is a robust enough to cluster very complex data points based on predefined gap (which we have made this gap in meter scale and set it to 50 as default and can easily adjusted to any other number). The freedom of choosing gap between clusters made this algorithm the best candidate for this project. 
This script creates main clusters using DBSCAN algorithm. Then it sums up the SAIDI indices,  number of affected customers and tree quantity in each cluster. Finally it creates deciles for mentioned aggregated values. These deciles can be used to identify high risk clusters/streets. According to discussions between Qrious and Vector, Vector expressed their interest to see the results up to street level. This will cause to have high accuracy clustering up to one street or multiple close streets but also ending up with lots of outliers (outliers are labelled as 'no cluster' in cluster columns ). To meet this requirement, the gap between clusters has been set to 50 meters. However with adjustable parameters provided in the scripts, it is possible to  change the scale up to region level by changing the gap up to 100 meters, 200 meters or more. 
In index.py all the thresholds related to deciles can be adjusted. These thresholds are used to identify high risk clusters/streets. The gap for DBSCAN algorithm also can be adjusted in index.py. The default value is 50 meters. 
The outcome of this clustering can be view in a generated map called 50m_cluster_map.html  Members of each cluster have the same colour. Polygons are trees and squares are power poles. Points with very small size (seen as dots) are outliers. Top n critical clusters/streets are marked with three colours. Red, green and blue colours on markers define high, medium and low level of  risk among selected top n critical clusters/streets.	
 
## Step 2 - Subclustering
Subclustering can be done based on two different date columns namely CTNIssueDate and TreeNotificationCompletionFullDate. In order to define which one these two date columns needs to be used, its enough to set TreeNotificationCompletionFullDate_subclustering to True or False. 
subclustering.py 
subclustering_TreeNotificationCompletionFullDate.py
### Inputs 
- clusterList.csv (this is the list generated by clustering.py in step 1) 
### Outputs 
- subclusters_monthToAccident.csv 
As we mentioned, main clusters formed in step 1, are clusters at street or very close streets level. The next step is to go another level deeper to see which trees are closer to power poles in each cluster. This helps us to have a more accurate prediction of which trees cause accidents on power poles. As trees and power  poles are very close to each other in each cluster, we can not use DBSCAN anymore as this requires to define gap for each separate cluster manually.  We chose the K-Means algorithm to do this job because in K-Means, clusters are formed based on the given number of centroids. The way this script defines number of centroids depends on number of power poles in each cluster.  For instance if in a cluster we have two power poles with multiple trees, we set two centroids for K-Means as ideally we look for two sub-clusters, having one power pole and some trees per each sub-cluster. K-Means does this job by measuring the distance using Euclidean distance and tries to create two most separable possible clusters. As a result, this script creates sub-clusters using k-means algorithm to find closest trees and power poles in each cluster. The number of months from the CTNIssueDate/TreeNotificationCompletionFullDate till the closest power poles with reported accident in each sub-cluster is calculated and if there are multiple trees around a power pole in a sub-cluster, the tree with shortest interval to accident is selected as the one with highest possibility of causing the accident. By setting includeNoClusters to True or False the model will include or exclude nodes with No Cluster lable (outliers) in subclustering process.

## Step 3 - Subclustering Preventive Maintainance
The purpose of this subclustering, is to create subclusters for clusters which have no incident and contain only CTN jobs. CTN jobs on trees in these clusters are detected as successful preventive maintainance jobs as no incident has been reported around them till current date or any given date. This subclustering can be done using two different date columns namely CTNIssueDate and TreeNotificationCompletionFullDate. In order to define which one these two date columns needs to be used, its enough to set TreeNotificationCompletionFullDate_subclustering_maintainance to True or False. 
subclustering_preventive_maintainance.py 
subclustering_preventive_maintainance_TreeNotificationCompletionFullDate.py
### Inputs: 
- clusterList.csv (this is the list generated by clustering.py in step 1) 
### Outputs: 
- subclusters_preventiveMaintainance_monthsAgo.csv 
The subclustering in this section is similar to what explained earlier in step 2. The only difference is that, in this suclustering, there is no incident around CTN jobs so there is no month to accident. The only thing here is to count months_ago value which is  number of months since CTNIssueDate/TreeNotificationCompletionFullDate till current date or any given date. To define whether it is desired to use current date or any other date to cound month_ago value, set monthsPassedTill to 'current_date' or to YYYY-mm-DD format date string.

## Step 4 - survival_analysis
### Inputs 
- subclusters_monthToAccident.csv (generated by subclustering.py or subclusteringTreeNotificationCompletionFullDate.py at step 2)
- subclusters_preventiveMaintainance_monthsAgo.csv (generated by subclustering_preventive_maintainance.py or subclustering_preventive_maintainance_TreeNotificationCompletionFullDate.py )

### Outputs 
- survival_rate.csv (main outcome of this script which can be used for visualizations and final analysis)
- least_effective_preventions_TreeMonthBased.csv (list of trees and number of months with least effective CTN job)
- tree_based_prevention_agg.csv (aggregated all survival analysis at tree type level)
- least_effective_preventions_TreeBased.csv csv (list of trees with least effective CTN job)

The survival analysis helps to measure the success rate of preventive maintainance job toward stopping any potential incident. In order to prepare data for this model, results from  subclusters_monthToAccident.csv are aggeragated at tree and months_to_accident level and then a new column is built called tree_month which is a result of concatenating tree type to months_to_accident (e.g. 'Kanuka 11' stands for values for kanuka, 11 months to accident). On the other hand results from  subclusters_preventiveMaintainance_monthsAgo.csv are aggeragated at tree and months_ago_maintained level and agin a new column is built called tree_month which is a result of concatenating tree type to months_ago (e.g. 'Kanuka 11' stands for values for kanuka, 11 months ago). Finally tree_month column on both sides is used to join the tables and get total number of trees and measure prevention success rate by dividing number of trees which have been trimmed successfully by total number of trees. This is done for all trees and all the related number of months. As the CTN data covers only 41 records from last 9 months, we need to ignore the matches between 9 months_ago_maintained and 9 months_to_accident. Because we have lot of  months_to_accident=9 but not only few months_ago_maintained=9. The number of months that needs to be ignored also can be adjusted via ignoreNmonthsPass parameter which is set to 9 as default.
This script also detects least effective trees and least effective trees and months by using created deciles around prevent success rate and total number of trees whichare  defined clearly in index.py and can be easily adjusted via total_tree_threshold and prevent_success_rate_decile parameters. For further examples and analysis please refer to slides provided to Vector on last week. 

## Step 5 - tfidf_vectorize_description
### Inputs: 
- subclusters_monthToAccident.csv (generated by subclustering.py at step 2)

### Outputs: 
- subclusters_monthToAccident_keywords.csv 
This script takes subclusters_monthToAccident.csv and uses WorkRequiredDescription  column for detecting important keywords. 
The complete text mining operations are applied on WorkRequiredDescription . It consists removing stop words, punctuations, lemmatization and stemming. The vocabulary of used packages recognize only a standard set of words as stop words. For this project, another set of stop words can be defined and added on top of standard package such as 'power', 'tree', 'notice'. This list of stop words can be changed by adding or removing words to manualStopWordList.
The other important parameter that can be set by user is selectFromSelectedKeywords . If it is set as True, the script creates a column called Work Type. This column determines a work category based on predefined lists of keywords. These lists also can be adjusted by removing keywords or adding more keywords. As the used package for lemmatization and stemming are not complete enough, we added set of important verbs which have the same root in order to cope with the incompleteness issue of used Python packages.  The default given keywords are selected based on the outcome of TF-IDF algorithm (which defines important keywords). 
If selectFromSelectedKeywords is set to False, then the script creates binary columns for top n keywords. The number of top keywords can be set at topNKeywords. However this can only be helpful if Vector is interested in doing further separate analysis only. If this parameter is set to False, some of the next steps might fail. 
In short, for this script, it is not recommended to change parameters too much as the default values are chosen based on lot of evaluations and experiments to choose the best fits for this project. 

## Step 6 - tree_based_deciles
### Inputs: 
- subclusters_monthToAccident.csv (generated by subclustering.py at step 2)
### Outputs: 
- tree_based_aggregation.csv 
- top_deciles_critical_trees.csv
This script uses all the numbers calculated at clustering and sub-clustering steps and aggregates them at type of tree level. It calculates: 
-  median of cluster_SAIDI_decile (which was calculated at clustering, step1), 
- median of months_to_accident (which was calculated at sub-clustering, step 2), 
- sum of TreeQuantity, 
- count of unique geographical locations (this helps to see how well the collected data for each tree is distributed. Is it all accumulated at limited number of geographical locations? Or distributed across the country).   
Finally it creates deciles around all these aggregated values mentioned above. These deciles are used to define top critical trees. By default, tree_quantity_decile_threshold has been set to 2 to give a chance to trees with low number of quantities to be top n critical tree selection process. The median aggregations around cluster_SAIDI_decile and months_to_accident also helps to select top n trees with ignoring their quantity. However, the distribution decile helps us to select those trees which their data have been collected from different locations to have a higher chance of being in top n critical list, rather then those with limited locations or accumulated at limited locations.
The CSV files generated by this script as output provides all of the aggregated values at Type of Tree level and a list of selected top critical trees based on the predefined thresholds.  

## Step 7 - categorical_scaterplot
### Inputs: 
- top_deciles_critical_trees.csv (generated at step 4)
- subclusters_monthToAccident_keywords.csv (generated at step 3)
### Outputs: 
- boxplot.png or violinPlot.png 
This script can generate two types of plots to show how is the distribution of top critical trees across number_of_months since a Work Type has been done. The colours on dots represent Work Type. By setting plotType to 'boxPlot'  or  'violinPlot'. 

## Step 8 - mapping_critical_trees
### Inputs: 
- tree_based_aggregation.csv 
- top_deciles_critical_trees.csv
### Outputs: 
- n .html maps for top n critical. 
This script generates n number of html maps for top n critical trees which were defined at step 4. The size of points on maps represents the quantity of trees and the colour opacity represents median SAIDI decile(at tree level). Points with having any SAIDI index associated to them can be found in red colour, and points with no SAIDI index linked to them are shown in blue colour. Consequently, the bigger points with higher opacities in red colour are the most critical areas where the tree caused major accidents.
 
## Survival_charts
### Inputs: 
- Survival_rate.csv (generated at step 4)
### Outputs: 
- quartile_barchart_median_success_rate.png
- quartile_scaterplot_success_rate.png
- quartile_success_rate_distribution.png
- quantile_barchart_median_success_rate.png
- quantile_scaterplot_success_rate.png
- quantile_success_rate_distribution.png
- scaterplot_barchart_median_success_rate.png
- scaterplot_success_rate.png
- success_rate_distribution.png 
This script can generate three types of plots namel distribution charts, bar charts and scaterplots based on real numbers, quantiles and quartiles for months_passed versue success_rate. 

